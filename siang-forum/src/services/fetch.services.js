export const  getSign = async () => {
    return await fetch("http://sandipbgt.com/theastrologer/api/sunsigns")
        .then((response) => response.json())
};

export const getHoroscope = async (sign, time) => {
    return await fetch(`http://sandipbgt.com/theastrologer/api/horoscope/${sign}/${time}`)
        .then((response) => response.json()) 
        .then(({ horoscope }) => horoscope)
};

export const fetchTarot = async (name, desc) => {
    const response = await fetch(`https://tarot-api.onrender.com/api/v1/cards/random?n=10`)
    //  console.log('fetch');
     return response.json()
    }
    