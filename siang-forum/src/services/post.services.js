import { get, push, ref, update } from 'firebase/database'

import { db } from '../firebase/config'

export const formPostDocument = (snapshot) => {
	const PostDocument = snapshot.val()

	return Object.keys(PostDocument).map((key) => {
		const post = PostDocument[key]

		return {
			...post,
			id: key,
			addedOn: new Date(post.addedOn),
			likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
		}
	})
}
export const getPostsByAuthor = async (username) => {
	const snapshot = await get(ref(db, `users/${username}/posts`))

	if (!snapshot.exists()) return []


	return Object.values(snapshot.val())
}

export const addPost = async (category, title, content, username, avatarUrl) => {
	const post = {
		category: category,
		title,
		content, 
		author: username,
		avatarUrl: avatarUrl,
		addedOn: Date.now(),
	}

	const { key } = await push(ref(db, 'posts'), post)

	return update(ref(db), {
		[`users/${username}/posts/${key}`]: true,
	})
}

export const getAllPosts = async () => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) {
		return []
	}

	return Object.keys(snapshot.val()).map((key) => ({ ...snapshot.val()[key], id: key }))
}

export const getPostByTitle = async (query) => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((post) => post.title.toLowerCase().includes(query.toLowerCase()))
}

export const getPostByText = async (query) => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((post) => post.content.toLowerCase().includes(query.toLowerCase()))
}


export const getPostByCategory = async (query) => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((post) => post.category.toLowerCase().includes(query.toLowerCase()))
}

export const getPostByAuthor = async (query) => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((post) => post.author.toLowerCase().includes(query.toLowerCase()))
}

export const getPostById = async (id) => {
	const snapshot = await get(ref(db, `posts/${id}`))
	if (!snapshot.exists()) throw new Error(`Post with id: ${id} does not exist!`)

	return {
		...snapshot.val(),
		id,
	}
}
export const editPostTitle = async (postId, title) => {
	return update(ref(db), {
		[`posts/${postId}/title`]: title,
	})
}

export const editPostContent = async (postId, content) => {
	return update(ref(db), {
		[`posts/${postId}/content`]: content,
	})
}

export const togglePostLikes = async (postId, author, like = true) => {
	return update(ref(db), {
		[`users/${author}/likedPosts/${postId}`]: like || null,
		[`posts/${postId}/likedBy/${author}`]: like || null,
	})
}

export const searchInPosts = async (query) => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((post) => post.content.toLowerCase().includes(query.toLowerCase()) || post.title.toLowerCase().includes(query.toLowerCase()) || post.author.toLowerCase().includes(query.toLowerCase()))
}
export const deletePost = async (postId, username) => {
	return await update(ref(db), {
		[`posts/${postId}`]: null,
		[`users/${username}/posts/${postId}`]: null,
	})
}

export const getAllPostsNum = async () => {
	const snapshot = await get(ref(db, 'posts'))

	if (!snapshot.exists()) {
		return []
	}

	return Object.keys(snapshot.val())
}

export const getPostsByAuthorSinglePost = async (username) => {
	const snapshot = await get(ref(db, `users/${username}/posts`))

	if (!snapshot.exists()) return []
	const result = Object.keys(snapshot.val()).length

	console.log(result)
	return Object.keys(snapshot.val())
}
