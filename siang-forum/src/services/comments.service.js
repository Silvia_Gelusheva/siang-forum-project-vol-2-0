import { get, push, ref, update } from 'firebase/database'

import { db } from '../firebase/config'

export const addComment = async (author, avatarUrl, postId, content) => {
	const comment = { author, avatarUrl, postId, content, createdOn: Date.now() }
	const result = await push(ref(db, 'comments'), comment)
	await update(ref(db), {
		[`users/${author}/comments/${result.key}`]: true,
		[`posts/${postId}/comments/${result.key}`]: true,
	})

	return { ...comment, id: result.key }
}

export const getCommentById = async (id) => {
	const snapshot = await get(ref(db, `comments/${id}`))

	if (!snapshot.exists()) throw new Error(`Comment with id ${id} does not exist!`)

	return {
		...snapshot.val(),
		id,
	}
}

export const deleteComment = async (commentId, username, postId) => {
	return await update(ref(db), {
		[`comments/${commentId}`]: null,
		[`posts/${postId}/comments/${commentId}`]: null,
		[`users/${username}/comments/${commentId}`]: null,
	})
}

export const getAllComments = async () => {
	const snapshot = await get(ref(db, 'comments'))

	if (!snapshot.exists()) {
		return []
	}

	return Object.keys(snapshot.val()).map((key) => ({ ...snapshot.val()[key], id: key }))
}
