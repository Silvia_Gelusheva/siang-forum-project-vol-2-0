import { async } from '@firebase/util'
import { ref, push, get, set, update, query, equalTo, orderByChild, orderByKey } from 'firebase/database'
import { db } from '../firebase/config'
// import { API } from '../common/constants'

export const addMember = async (email, username, firstname, lastname) => {
	const member = {
		firstname,
		lastname,
		email,
		addedOn: Date.now(),
	}

	const { key } = await push(ref(db, 'members'), member)

	return update(ref(db), {
		[`users/${username}/members/${key}`]: true,
		[`users/${addedOn}/members/${key}`]: true,
	})
}

export const getAllMembers = async () => {
	const snapshot = await get(ref(db, 'users'))

	if (!snapshot.exists()) {
		return []
	}

	return Object.keys(snapshot.val()).map((key) => ({ ...snapshot.val()[key], id: key }))
}

// export const getMemberByUsername = async (query) => {
//   const snapshot = await get(ref(db, 'users'))

//   if (!snapshot.exists()) throw new Error('Something went wrong.')

//   return Object
//     .keys(snapshot.val())
//     .map(key => ({...snapshot.val()[key], id: key}))
//     .filter(u => u.username.toLowerCase().includes(query.toLowerCase()))
// }

// export const getMemberByName = async (query) => {
//     const snapshot = await get(ref(db, 'users'))

//     if (!snapshot.exists()) throw new Error('Something went wrong.')

//     return Object
//       .keys(snapshot.val())
//       .map(key => ({...snapshot.val()[key], id: key}))
//       .filter(u => u.firstname.toLowerCase().includes(query.toLowerCase()))
//   }

export const searchInMembers = async (query) => {
	const snapshot = await get(ref(db, 'users'))
	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((u) => u.firstname.toLowerCase().includes(query.toLowerCase()) || u.lastname.toLowerCase().includes(query.toLowerCase()) || u.username.toLowerCase().includes(query.toLowerCase()))
}

export const getMemberByName = async (query) => {
	const snapshot = await get(ref(db, 'users'))

	if (!snapshot.exists()) throw new Error('Something went wrong.')

	return Object.keys(snapshot.val())
		.map((key) => ({ ...snapshot.val()[key], id: key }))
		.filter((u) => u.username.toLowerCase().includes(query.toLowerCase()))
}

export const getAllUsers = async () => {
	const snapshot = await get(ref(db, 'users'))

	if (!snapshot.exists()) {
		return []
	}
	return Object.keys(snapshot.val())
}

