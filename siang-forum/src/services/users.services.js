import { ref, get, set, update, query, equalTo, orderByChild } from 'firebase/database'
import { db } from '../firebase/config'
import { userRole } from '../common/enums/user-role.enum'

export const getUser = async (username) => {
	const snapshot = await get(ref(db, `users/${username}`))

	return snapshot.val()
}

export const getUserById = async (uid) => {
	const snapshot = await get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)))

	const { comments, posts, likedPosts, ...value } = snapshot.val()?.[Object.keys(snapshot.val())?.[0]]

	return {
		...value,
		commentIds: comments ? Object.keys(comments) : [],
		postIds: posts ? Object.keys(posts) : [],
		likedPostsIds: likedPosts ? Object.keys(likedPosts) : [],
	}
}

export const createUser = async (uid, firstname, lastname, username, isActive = true, role = userRole.BASIC) => {
	const user = await getUser(username)

	if (user !== null) throw new Error(`Username ${username} already exists!`)

	const userData = { uid, firstname, lastname, username, isActive, role, registeredOn: Date.now() }

	await set(ref(db, `users/${username}`), userData)

	return { ...userData, commentIds: [], postIds: [], likedPostsIds: [], bornIn: "", livesIn: "", education: "", workplace: "", hobbies: "" }
}

export const updateUserProfilePic = (username, url) => {
	return update(ref(db), {
	  [`users/${username}/avatarUrl`]: url,
	})
  }

export const updateUserCoverPic = (username, url) => {
	return update(ref(db), {
		[`users/${username}/coverUrl`]: url,
	})
}
export const editFirstName = async (username, firstname) => {
	return update(ref(db), {
		[`users/${username}/firstname`]: firstname,
	})
}

export const editLastName = async (username, lastname) => {
	return update(ref(db), {
		[`users/${username}/lastname`]: lastname,
	})
}

export const editBornIn = async (username, bornIn) => {
	return update(ref(db), {
		[`users/${username}/bornIn`]: bornIn,
	})
}
export const editLivesIn = async (username, livesIn) => {
	return update(ref(db), {
		[`users/${username}/livesIn`]: livesIn,
	})
}

export const editEducation = async (username, education) => {
	return update(ref(db), {
		[`users/${username}/education`]: education,
	})
}

export const editWorkPlace = async (username, workplace) => {
	return update(ref(db), {
		[`users/${username}/workplace`]: workplace,
	})
}
export const editHobbies = async (username, hobbies) => {
	return update(ref(db), {
		[`users/${username}/hobbies`]: hobbies,
	})
}















export const blockUser = async (username) => {
	const snapshot = await get(query(ref(db, 'users'), orderByChild('username'), equalTo(username)))
	const value = snapshot.val()

	if (value !== null) {
		const key = Object.keys(value)[0]
		let currentStatus = value[key].isActive

		if (value[key].isActive === true) {
			currentStatus = false
		} 
	
		return (
			update(ref(db), {
				[`users/${value[key].username}/isActive`]: currentStatus,
			}),
			value[key]
		)
	}
}

export const unblockUser = async (username) => {
	const snapshot = await get(query(ref(db, 'users'), orderByChild('username'), equalTo(username)))
	const value = snapshot.val()

	if (value !== null) {
		const key = Object.keys(value)[0]
		let currentStatus = value[key].isActive

		if (value[key].isActive === false) {
			currentStatus = true
		}

		return (
			update(ref(db), {
				[`users/${value[key].username}/isActive`]: currentStatus,
			}),
			value[key]
		)
	}
}
