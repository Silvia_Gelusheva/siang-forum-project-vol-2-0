import { useEffect, useMemo, useState } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";

import { AppContext } from "../../context/app.context";
import { COLUMNS_MEMBERS } from "../../components/MembersGroup/ColumnsMembers";
import { GlobalFilter } from "../../components/GlobalFilter/GlobalFilter";
import { Pagination } from "../../components/MembersGroup/Pagination";
import { getAllMembers } from "../../services/members.services";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";

export const MembersTable = () => {
  const [users, setUsers] = useState([]);
  const { userData } = useContext(AppContext);
  const navigate = useNavigate();

  useEffect(() => {
    getAllMembers()
      .then(setUsers)
      .then(console.log(users))
      .catch((e) => console.log(e.message));
  }, []);

  const columns = useMemo(() => COLUMNS_MEMBERS, []);
  const data = useMemo(() => users, [users]);
  const handleRowClick = (row) => {
    navigate(`/members/profile/${row.original.username}`);
  };

  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: { pageSize: 5 },
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    pageOptions,
    state,
    setGlobalFilter,
    gotoPage,
    pageCount,
    setPageSize,
    prepareRow,
  } = tableInstance;
  const { pageIndex, pageSize, globalFilter } = state;

  return (
    <div>
      {userData?.isActive === false && navigate("/blocked")}

      <div className="flex justify-center mt-5">
        <div className="w-[90%] lg:absolute lg:flex flex-col">
          <div className="flex justify-center lg:justify-end lg:mt-0 mt-5 mb-5">
            <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
          </div>
          <table className="w-full table opacity-90 flex" {...getTableProps()}>
            <thead>
              {headerGroups.map((hg) => (
                <tr {...hg.getHeaderGroupProps()}>
                  {hg.headers.map((column) => (
                    <th
                      className="p-3 font-bold bg-primary text-lg text-center uppercase hidden lg:table-cell"
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render("Header")}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? " 🔻"
                            : "🔺"
                          : ""}
                      </span>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>

            <tbody {...getTableBodyProps()}>
              {page.map((row) => {
                prepareRow(row);
                return (
                  <tr 
                  className="bg-inherit flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-8 lg:mb-0 lg:w-full"

                    {...row.getRowProps()}
                    onClick={() => handleRowClick(row)}
                  >
                    {row.cells.map((cell) => (
                      <td
                        className="flex flex-col lg:flex-row w-full bg-secondary whitespace-pre-line lg:w-auto px-10 py-1 lg:table-cell cursor-pointer relative lg:static"
                        {...cell.getCellProps()}
                      >
                        {" "}
                        {cell.render("Cell")}
                      </td>
                    ))}
                  </tr>
                );
              })}
            </tbody>
          </table>

          <div className="flex flex-col lg:flex-row w-full justify-center items-center text-sm px-5 py-5 mb-8 text-center">
            <div className="flex">
              <span className=" bg-primary font-bold text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg">
                Page{" "}
                <strong>
                  {pageIndex + 1} of {pageOptions.length}
                </strong>{" "}
              </span>
              <span className="bg-primary font-bold text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg">
                Go to page:{" "}
                <input
                  type="number"
                  defaultValue={pageIndex + 1}
                  onChange={(e) => {
                    const pageNumber = e.target.value
                      ? Number(e.target.value) - 1
                      : 0;
                    gotoPage(pageNumber);
                  }}
                  style={{ width: "50px" }}
                />
              </span>
              <select
                className="font-bold bg-primary text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg"
                value={pageSize}
                onChange={(e) => setPageSize(Number(e.target.value))}
              >
                {[3, 5, 8].map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    {" "}
                    Show {pageSize}
                  </option>
                ))}
              </select>
            </div>

            <div className="flex">
              <button
                className="font-bold bg-primary text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg"
                onClick={() => gotoPage(0)}
                disabled={!canPreviousPage}
              >
                {" "}
                {"<<"}
              </button>
              <button
                className="font-bold bg-primary text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg"
                onClick={() => previousPage()}
                disabled={!canPreviousPage}
              >
                Prev
              </button>

              <button
                className="font-bold bg-primary text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg"
                onClick={() => nextPage()}
                disabled={!canNextPage}
              >
                Next
              </button>
              <button
                className="font-bold bg-primary text-sm lg:px-5 lg:py-2.5 text-center mr-2 mb-2 rounded-lg"
                onClick={() => gotoPage(pageCount - 1)}
                disabled={!canNextPage}
              >
                {" "}
                {">>"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
