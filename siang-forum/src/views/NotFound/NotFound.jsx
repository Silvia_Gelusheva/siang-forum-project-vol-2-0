// import { HelmetProvider } from 'react-helmet-async'

export default function NotFound() {
	return (
		<div className="hero min-h-screen" style={{ backgroundImage: `url("/src/assets/tree1.png")` }}>
  <div className="hero-overlay bg-opacity-20"></div>
  <div className="hero-content text-center text-neutral-content">
  <div className='card bg-fuchsia-50 shadow-2xl col-auto'>
				<title>Not Found</title>
		 <p className='text-xl text-primary text-center font-bold shadow-2xl p-32'>Page not found!</p>
		</div>
  </div>
</div>
	
	)
}
