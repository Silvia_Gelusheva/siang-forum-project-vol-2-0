import { deleteComment, getCommentById } from "../../services/comments.service";
import {
  deletePost,
  editPostContent,
  editPostTitle,
  getPostById,
  getPostsByAuthorSinglePost,
  togglePostLikes,
} from "../../services/post.services";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import AddComment from "../../components/SinglePostGroup/AddComment";
import { AppContext } from "../../context/app.context";
import Comment from "../../components/SinglePostGroup/Comment";
import CurrentPost from "../../components/SinglePostGroup/CurrentPost";
import { addComment } from "../../services/comments.service";

export default function SinglePost() {
  const { postId } = useParams();
  const [toggle, setToggle] = useState(false);
  const { addToast, userData, setAppState, user, theme } =
    useContext(AppContext);
  const navigate = useNavigate();

  const [state, setState] = useState({
    post: null,
    title: null,
    content: null,
    comments: [],
    showCommentForm: false,
    showEditTitle: false,
    showEditContent: false,
    titleText: "",
    contentText: "",
    commentText: "",
  });

  const [postsByAuthor, setPostsByAuthor] = useState([]);

  useEffect(() => {
    getPostsByAuthorSinglePost(state?.post?.author)
      .then((postsByAuthor) => setPostsByAuthor(postsByAuthor))
      .catch((error) => addToast("error", error.message));
  }, []);

  useEffect(() => {
    getPostById(postId)
      .then((currentPost) => {
        setState((prev) => ({
          ...prev,
          post: currentPost,
          titleText: currentPost.title,
          contentText: currentPost.content,
        }));
        const ids = Object.keys(currentPost.comments || {});

        return Promise.all(ids.map(getCommentById)).then((comments) =>
          setState((state) => ({
            ...state,
            comments,
          }))
        );
      })
      .catch((error) => addToast("error", error.message));
  }, []);

  const addPostComment = () => {
    if (state.commentText === "") {
      return setState({ ...state, showCommentForm: false });
    }

    addComment(userData.username, userData.avatarUrl, postId, state.commentText)
      .then((comment) => {
        setState({
          ...state,
          comments: [...state.comments, comment],
          commentText: "",
          showCommentForm: false,
        });
        setToggle(!toggle);
      })
      .catch((error) => addToast("error", error.message));
  };

  const saveTitle = async () => {
    if (state.titleText === "" || state.titleText === state.post.title)
      return setState({ ...state, showEditTitle: false });

    try {
      await editPostTitle(state.post.id, state.titleText);

      addToast("success", "Post title was updated successfully!");
      setState({
        ...state,
        showEditTitle: false,
        post: {
          ...state.post,
          title: state.titleText,
        },
      });
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const saveContent = async () => {
    if (state.contentText === "" || state.contentText === state.post.content)
      return setState({ ...state, showEditContent: false });

    try {
      await editPostContent(state.post.id, state.contentText);
      addToast("success", "Post content was updated successfully!");

      setState({
        ...state,
        showEditContent: false,
        post: {
          ...state.post,
          content: state.contentText,
        },
      });
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const toggleLike = async () => {
    try {
      await togglePostLikes(
        state.post.id,
        userData.username,
        !userData.likedPostsIds.includes(state.post.id)
      );

      if (userData.likedPostsIds.includes(state.post.id)) {
        setAppState({
          theme,
          user,
          userData: {
            ...userData,
            likedPostsIds: userData.likedPostsIds.filter(
              (id) => id !== state.post.id
            ),
          },
        });

        delete state.post.likedBy[userData.username];
        setState({
          ...state,
          post: state.post,
        });
      } else {
        setAppState({
          theme,
          user,
          userData: {
            ...userData,
            likedPostsIds: [...userData.likedPostsIds, state.post.id],
          },
        });

        if (!state.post.likedBy) {
          state.post.likedBy = {};
        }

        state.post.likedBy[userData.username] = true;
        setState({
          ...state,
          post: state.post,
        });
      }
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const removePost = (postId, username) => {
    deletePost(postId, username)
      .then(() => {
        setState((state.post = null));
        navigate("/");
      })
      .catch((error) => addToast("error", error.message));
  };

  const removeComment = (commentId, username, postId) => {
    deleteComment(commentId, username, postId)
      .then(() => {
        setState((prev) => ({
          ...prev,
          comments: prev.comments.filter((c) => c.id !== commentId),
        }));
      })
      .catch((error) => addToast("error", error.message));
  };

  return (
    <div className="flex flex-col justify-center items-centers">
      {/* post content */}
      <CurrentPost
        state={state}
        setState={setState}
        saveTitle={saveTitle}
        saveContent={saveContent}
        toggle={toggle}
        toggleLike={toggleLike}
        setToggle={setToggle}
        author={state?.post?.author}
        avatarUrl={state?.post?.avatarUrl}
        addedOn={new Date(state?.post?.addedOn).toLocaleDateString("en-GB")}
        postId={postId}
        removePost={removePost}
      />
      {/* add comment */}
      {toggle ? (
        <AddComment
          addPostComment={addPostComment}
          state={state}
          setState={setState}
        />
      ) : null}

      {/* comments */}
      {state?.comments?.length === 0 ? (
        <div>
          <div className="divider"></div>
          <p className="flex justify-center font-semibold">
            Be the first to comment this post!
          </p>
        </div>
      ) : (
        state?.comments?.map((c) => (
          <Comment
            key={c.id}
            commentId={c.id}
            author={c.author}
            avatarUrl={c.avatarUrl}
            comment={c.content}
            addedOn={c.createdOn}
            postId={postId}
            removeComment={removeComment}
          />
        ))
      )}
    </div>
  );
}
