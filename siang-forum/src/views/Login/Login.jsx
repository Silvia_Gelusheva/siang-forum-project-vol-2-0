import * as Yup from "yup";

import { ErrorMessage, Field, Form, Formik } from "formik";
import { useLocation, useNavigate } from "react-router-dom";

import { AppContext } from "../../context/app.context";
import { loginUser } from "../../services/auth.service";
import { useContext } from "react";

export default function Login() {
  const { addToast, setAppState, ...appState } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();

  const initialValues = {
    email: "",
    password: "",
  };
  const onSubmit = (values) => {
    login(values);
  };

  const validationSchema = Yup.object({
    email: Yup.string().required("Email is required!").email("Invalid email"),
    password: Yup.string().required("Password is required!"),
  });

  const login = async (values) => {
    try {
      const credentials = await loginUser(values.email, values.password);

      setAppState({
        ...appState,
        user: {
          email: credentials.user.email,
          uid: credentials.user.uid,
        },
      });
      addToast("success", "You have been logged!");
      location?.state?.from?.pathname || navigate("/");
    } catch (error) {
      addToast("error", error.message);
    }
  };

  return (
    <div className="relative mx-auto w-[90%] lg:max-w-lg bg-secondary shadow-xl px-6 pt-5 pb-8 sm:rounded-xl sm:px-10 mt-32">
      <div className="max-w-md mx-auto bg-secondary rounded-lg overflow-hidden md:max-w-md">
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {() => (
            <Form>
              <div className="md:flex">
                <div className="w-full p-3 px-6 py-10">
                  <div className="text-center">
                    <span className="text-2xl text-neutral font-bold">LogIn</span>
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="email"
                      placeholder="email"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="email"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="password"
                      type="password"
                      placeholder="password"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="password"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4">
                    <button
                      className="h-12 w-full bg-success text-neutral rounded hover:bg-accent"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
