import FetchedHoroscope from "../../components/FetchedHoroscope/FetchedHoroscope";
import SelectedSign from "../../components/FetchedHoroscope/SelectedSign";
import SelectedTimeFrame from "../../components/FetchedHoroscope/SelectTimeFrame";
import { useState } from "react";
export default function Horoscope() {
  const [selectedSign, setSelectedSign] = useState(null);
  const [timeFrameSelected, setTimeFrameSelected] = useState(null);

  const restart = () => {
    setSelectedSign(null);
    setTimeFrameSelected(null);
  };

  return (
    <div className="flex flex-col items-center justify-center mx-6 my-10 lg:mt-24">
      {!selectedSign && <SelectedSign onSignSelected={setSelectedSign} />}
      {selectedSign && !timeFrameSelected && (
        <SelectedTimeFrame onTimeFrameSelected={setTimeFrameSelected} />
      )}
      {selectedSign && timeFrameSelected && (
        <FetchedHoroscope sign={selectedSign} time={timeFrameSelected} />
      )}
      {selectedSign && (
        <div className="flex items-center justify-end mt-8">
          <button
            className="btn bg-black border-none shadow-sm mr-2 text-primary"
            onClick={restart}
          >
            BACK
          </button>
        </div>
      )}
    </div>
  );
}
