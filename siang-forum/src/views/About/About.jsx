import "./About.css";
export default function About() {
  return (
    <div
      className="hero min-h-screen"
      style={{
        backgroundImage: `url("https://www.pixel4k.com/wp-content/uploads/2018/10/neon-sunset-4k_1540749914.jpg.webp")`,
      }}
    >
      <div className="hero-overlay bg-opacity-20"></div>
      <div className="hero-content text-center text-primary">
        <div className="container w-[80%] box-shadow border-box py-8  px-12 bg-gray-200 opacity-75 rounded">
          <h2 className="uppercase font-bold text-secondary pt-1">Forum rules and information: </h2>
          <br />
          <p className="text-sm">
            {" "}
            The forum is a place where people can interact and have discussions
            about different topics. We ask that you follow these guidelines to
            ensure that the forums have some productive conversation. These
            rules and guidelines are enforced by administrators, and at their
            discretion they may delete posts without warning that do not comply.
            Also, failure to comply with these rules or our code of conduct may
            result in a ban from the forum.{" "}
          </p>
          <br />
          <p className="text-sm font-semibold">
            {" "}
            We rely on all forum members to help keep these discussion forums a
            safe place for people to share and view information. To do this, we
            request that all members comply with the following rules when
            contributing to the discussion forums:
          </p>
          <ul className="">
            <li>
              <p> Keep it friendly</p>
            </li>
            <li>
              <p>
                Be courteous and respectful. Appreciate that others may have an
                opinion different from yours.{" "}
              </p>
            </li>
            <li>
              <p>
                Stay on topic. When creating a new discussion thread, give a
                clear topic title and put your post in the appropriate category.
                When contributing to an existing discussion, try to stay 'on
                topic'. If something new comes up within a topic that you would
                like to discuss, start a new thread.{" "}
              </p>
            </li>
            <li>
              <p>
                {" "}
                Refrain from demeaning, discriminatory, or harassing behaviour
                and speech.
              </p>
            </li>
          </ul>
          <br />
          <p className="text-sm font-semibold">
            We maintain the rights to remove posts and threads to ensure that
            material posted in the discussion forums is not potentially harmful.
            For this reason, we may edit or choose not to publish any post,
            that:
          </p>
          <ul className="">
            <li>
              <p>
                {" "}
                contains disrespectful or derogatory remarks about any person
              </p>
            </li>
            <li>
              <p>
                {" "}
                contains advice or content that we believe is damaging,
                unhelpful or distressing to others
              </p>
            </li>
            <li>
              <p>
                {" "}
                contains swearing or offensive language, is nonsensical and/or
                irrelevant
              </p>
            </li>
          </ul>        
          <p className="flex text-md font-semibold justify-center mt-4 mb-4 text-secondary"> Your Admins: </p>
          <div className="flex flex-row justify-center gap-20">
          <a   href="https://www.linkedin.com/in/silvia-gelusheva-86424b4a/" target="_blank" className="flex flex-col items-center">           
            <img
              className="rounded-full w-20 h-20"
              src="/src/assets/si.png"
              alt=""
              
            />
          
                <p className="font-semibold text-secondary mt-1">Silvia Gelusheva</p> 
          </a>
          <a  href="https://www.linkedin.com/in/angelina-terzieva-bb2492192/" target="_blank" className="flex flex-col items-center">           
            <img
              className="rounded-full w-20 h-20"
              src="/src/assets/ang.jpg"
              alt=""
            
            />
               <p className="font-semibold text-secondary mt-1">Angelina Terzieva</p> 
               </a>
          </div>
        </div>
      </div>
    </div>
  );
}
