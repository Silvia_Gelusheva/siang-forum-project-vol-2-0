import { useContext, useEffect, useState } from "react";

import { AppContext } from "../../context/app.context";
import Avatar from "../../components/ProfileGroup/Avatar";
import BlockUser from "../../components/ProfileGroup/BlockUser";
import EditProfile from "../../components/ProfileGroup/EditProfile";
import Names from "../../components/ProfileGroup/Names";
import Statistics from "../../components/ProfileGroup/Statistics";
import Toggle from "../../components/ProfileGroup/Toggle";
import UserInfo from "../../components/ProfileGroup/UserInfo";
import { getPostsByAuthor } from "../../services/post.services";
import { getUser } from "../../services/users.services";
import { useParams } from "react-router-dom";

export default function Profile() {
  const { userData, addToast } = useContext(AppContext);
  const [posts, setPosts] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const { username } = useParams();
  const currentUserName = username ?? userData.username;
  const [toggle, setToggle] = useState(false);
  const [state, setState] = useState({
    profile: null,
    firstname: null,
    lastname: null,
    bornIn: null,
    livesIn: null,
    education: null,
    workplace: null,
    hobbies: null,
    showEditFirstName: false,
    showEditLastName: false,
    showEditBornIn: false,
    showEditLivesIn: false,
    showEditEducation: false,
    showEditWorkplace: false,
    showEditHobbies: false,
    firstNameText: "",
    lastNameText: "",
    bornInText: "",
    livesInText: "",
    educationText: "",
    workplaceText: "",
    hobbiesText: "",
  });

  useEffect(() => {
    getUser(currentUserName)
      .then((profile) => {
        setState((state) => ({
          ...state,
          profile,
          firstNameText: profile.firstname,
          lastNameText: profile.lastname,
          bornInText: profile.bornIn,
          livesInText: profile.livesIn,
          educationText: profile.education,
          workplaceText: profile.workplace,
          hobbiesText: profile.hobbies,
        }));
      })
      .catch((error) => addToast("error", error.message));
  }, []);

  useEffect(() => {
    getPostsByAuthor(currentUserName)
      .then(setPosts)
      .then(console.log(posts))
      .catch((error) => {
        console.log(error.message);
      });
  }, []);

  useEffect(() => {
    getUser(currentUserName)
      .then(setCurrentUser)
      .then(console.log(currentUser?.isActive));
  }, []);

  return (
    <div className="bg-gray-100">
      {/* {!userData?.isActive && navigate("/blocked")} */}
      <main className="profile-page rounded-2xl w-full">
        <section className="relative block" style={{ height: "400px" }}>
          <div className="absolute top-0 w-full h-full bg-center bg-base-100"></div>
        </section>
        <section className="relative py-1 bg-base-100">
          <div className="container mx-auto px-6">
            <div className="relative flex flex-col opacity-80 min-w-0 break-words bg-slate-100 w-full mb-8 shadow-xl rounded-lg -mt-64">
              <div className="px-6">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    {/* Avatar */}
                    <div className="relative">
                      <Avatar currentUser={currentUser} />
                    </div>
                  </div>

                  {/* Edit Profile Button */}
                  <div className="w-full lg:w-4/12 lg:order-3 lg:text-right lg:self-center flex justify-center">
                    <div className="mt-32 sm:mt-0">
                      {userData?.username === currentUser?.username && (
                        <Toggle
                          currentUser={currentUser}
                          toggle={toggle}
                          setToggle={setToggle}
                        />
                      )}
                    </div>

                    {/* Block user */}
                    {userData?.role === 2 &&
                    userData?.username !== currentUser?.username ? (
                      <div className="py-6 px-3 mt-32 sm:mt-0">
                        <BlockUser currentUser={currentUser} />
                      </div>
                    ) : null}
                  </div>

                  {/* Statistics */}
                  <div className="w-full lg:w-4/12 px-4 lg:order-1">
                    <div className="flex justify-center py-4 lg:pt-4 pt-8">
                      <Statistics currentUser={currentUser} />
                    </div>
                  </div>
                </div>
                {/* Name, LastName */}
                <Names state={state} />

                {/* Edit Profile Form */}
                {toggle ? (
                  <div className="border-t border-gray-300 text-justify-evenly">
                    <EditProfile
                      state={state}
                      setState={setState}
                      currentUserName={currentUserName}
                    />
                  </div>
                ) : (
                  <div className="w-full lg:w-full px-4 lg:order-1">
                    <div className="flex justify-center py-4 lg:pt-3 pt-8">
                      {/* Personal information */}
                      <UserInfo currentUser={currentUser} />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}
