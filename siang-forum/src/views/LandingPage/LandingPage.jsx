import { deletePost, getAllPosts } from "../../services/post.services";
import { useContext, useEffect, useMemo, useState } from "react";

import AddPost from "../../components/PostsNavigation/AddPost";
import Admins from "../../components/Admins/Admins";
import Advertisement from "../../components/Advertisement/advertisement";
import { AppContext } from "../../context/app.context";
import CreatePost from "../../components/CreatePost/CreatePost";
import Pagination from "../../components/PostsNavigation/Pagination";
import Post from "../../components/Post/Post";
import SearchPosts from "../../components/PostsNavigation/SearchPosts";
import SortPosts from "../../components/PostsNavigation/SortPosts";
import Stats from "../../components/Stats/Stats";
import TopMembers from "../../components/TopMembers/TopMembers";

export default function LandingPage() {
  const { addToast, userData, username, setAppState, ...appState } =
    useContext(AppContext);
  const [posts, setPosts] = useState([]);
  const [toggle, setToggle] = useState(false);
  const [sorted, setSorted] = useState({ sorted: "comments", reversed: false });
  const [query, setQuery] = useState("");

  useEffect(() => {
    getAllPosts()
      .then(setPosts)
      .catch((error) => addToast("error", error.message));
  }, []);

  const removePost = (postId, username) => {
    deletePost(postId, username)
      .then(() => {
        setPosts(posts.filter((p) => p.id !== postId));
      })
      .catch((error) => addToast("error", error.message));
  };

  const filteredPosts = useMemo(
    () =>
      posts.filter((post) => {
        return (
          post.title.toLowerCase().includes(query.toLowerCase()) ||
          post.content.toLowerCase().includes(query.toLowerCase()) ||
          post.author.toLowerCase().includes(query.toLowerCase())
        );
      }),
    [posts, query]
  );

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(5);
  const lastPostIndex = currentPage * postsPerPage;
  const firstPostIndex = lastPostIndex - postsPerPage;

  const currentPosts = filteredPosts.slice(firstPostIndex, lastPostIndex);
  const mostCommented = () => {
    setSorted({ sorted: "comments", reversed: !sorted.reversed });

    const postsCopy = [...posts];
    postsCopy.sort((postA, postB) => {
      const postAComments =
        postA.comments === undefined ? 0 : Object.keys(postA.comments).length;
      const postBComments =
        postB.comments === undefined ? 0 : Object.keys(postB.comments).length;
      if (sorted.reversed) {
        return postAComments - postBComments;
      }

      return postBComments - postAComments;
    });

    setPosts(postsCopy);
  };

  const mostLiked = () => {
    setSorted({ sorted: "likedBy", reversed: !sorted.reversed });

    const postsCopy = [...posts];
    postsCopy.sort((postA, postB) => {
      const postALikes =
        postA.likedBy === undefined ? 0 : Object.keys(postA.likedBy).length;
      const postBLikes =
        postB.likedBy === undefined ? 0 : Object.keys(postB.likedBy).length;
      if (sorted.reversed) {
        return postALikes - postBLikes;
      }

      return postBLikes - postALikes;
    });

    setPosts(postsCopy);
  };

  const mostRecent = () => {
    setSorted({ sorted: "addedOn", reversed: !sorted.reversed });

    const postsCopy = [...posts];
    postsCopy.sort((postA, postB) => {
      const postADate = new Date(postA.addedOn);
      const postBDate = new Date(postB.addedOn);
      if (sorted.reversed) {
        return postADate - postBDate;
      }

      return postBDate - postADate;
    });

    setPosts(postsCopy);
  };

  return (
    <div className="w-full">
      <div className="flex flex-col lg:flex-row justify-around mb-10">
        <div className="flex flex-col justify-between items-start lg:w-[70%] lg:h-[70%] bg-inherit mt-6">
          <div className="divider"></div>
          <div className="flex flex-col lg:flex-row justify-between w-full">
            <div className="flex ml-2 pb-2 lg:pb-0">
              <Pagination
                totalPosts={filteredPosts.length}
                postsPerPage={postsPerPage}
                setCurrentPage={setCurrentPage}
                currentPage={currentPage}
              />
            </div>
            <div className="flex flex-row">
              <SearchPosts query={query} setQuery={setQuery} />
              <AddPost toggle={toggle} setToggle={setToggle} />
              <SortPosts
                mostRecent={mostRecent}
                mostLiked={mostLiked}
                mostCommented={mostCommented}
              />
            </div>
          </div>

          <div className="divider"></div>
          <div className="container flex flex-col">
            {toggle ? (
              <CreatePost toggle={toggle} setToggle={setToggle} />
            ) : null}
          
            {currentPosts.map((post) => (
              <Post key={post.id} post={post} removePost={removePost} />
            ))}        
          </div>
        </div>
        <div className="flex flex-col justify-center items-center w-full lg:w-[20%] lg:h-[70%] mt-12">
          <Stats />
          <Admins />
          {/* <TopMembers /> */}
          <div className="divider"></div>
          <Advertisement />
        </div>
      </div>
    </div>
  );
}
