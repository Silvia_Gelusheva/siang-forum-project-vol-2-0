export default function Blocked() {
	return (
		<div className='card bg-primary shadow-2xl col-auto'>
						<title></title>
			<p className='text-xl text-fuchsia-100 text-center font-bold shadow-2xl p-32'>You've been blocked!</p>
		</div>
	)
}
