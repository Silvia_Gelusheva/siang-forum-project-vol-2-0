import * as Yup from "yup";

import { ErrorMessage, Field, Form, Formik } from "formik";
import { createUser, getUser } from "../../services/users.services";
import { useLocation, useNavigate } from "react-router-dom";

import { AppContext } from "../../context/app.context";
import { registerUser } from "../../services/auth.service";
import { useContext } from "react";

export default function Register() {
  const { addToast, setAppState, ...appState } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();

  const initialValues = {
    firstname: "",
    lastname: "",
    username: "",
    email: "",
    password: "",
  };
  const onSubmit = (values) => {
    register(values);
  };

  const validationSchema = Yup.object({
    firstname: Yup.string().required("firstname is required!"),
    lastname: Yup.string().required("lastname is required!"),
    username: Yup.string().required("username is required!"),
    email: Yup.string().required("email is required!").email("Invalid email"),
    password: Yup.string().required("password is required!"),
  });

  const register = async (values) => {
    try {
      const user = await getUser(values.username);

      if (user !== null)
        return addToast("error", `Username ${values.username} already exists!`);

      const credentials = await registerUser(values.email, values.password);

      try {
        const userData = await createUser(
          credentials.user.uid,
          values.firstname,
          values.lastname,
          values.username
        );

        setAppState({
          ...appState,
          userData,
        });

        addToast("success", "You have registered successfully!");

        navigate("/login");
      } catch (error) {
        return console.log(error.message);
      }
    } catch (error) {
      return console.log(error.message);
    }
  };

  return (
    <div className="relative mx-auto w-[90%] lg:max-w-lg bg-secondary shadow-xl px-6 pt-10 pb-8 ring-1 sm:rounded-xl sm:px-10 my-10">
      <div className="max-w-md mx-auto bg-secondary rounded-lg overflow-hidden md:max-w-md">
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {() => (
            <Form>
              <div className="md:flex">
                <div className="w-full p-3 px-6 py-10">
                  <div className="text-center">
                    <span className="text-2xl text-neutral font-bold">
                      Registration Form
                    </span>
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="firstname"
                      placeholder="firstname"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="firstname"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="lastname"
                      placeholder="firstname"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="lastname"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="username"
                      placeholder="username"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="username"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>
                  <div className="mt-4 relative">
                    <Field
                      name="email"
                      placeholder="email"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="email"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4 relative">
                    <Field
                      name="password"
                      type="password"
                      placeholder="password"
                      className="h-12 px-2 w-full border-2 rounded focus:outline-none focus:border-neutral"
                    />
                  </div>
                  <div className="errors">
                    <ErrorMessage
                      name="password"
                      component="span"
                      className="text-sm text-warning"
                    />
                  </div>

                  <div className="mt-4">
                    <button
                      className="h-12 w-full bg-success text-gray-600 rounded hover:bg-accent"
                      type="submit"
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
