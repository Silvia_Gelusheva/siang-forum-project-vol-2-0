export const API = `http://localhost:3000`
export const TITLE_MIN_LENGTH = 5
export const TITLE_MAX_LENGTH = 64
export const COMMENT_MIN_LENGTH = 32
export const COMMENT_MAX_LENGTH = 8192

export const USERNAME_MIN_LENGTH = 2
export const USERNAME_MAX_LENGTH = 32
export const USER_FIRST_NAME_MIN_LENGTH = 3
export const USER_FIRST_NAME_MAX_LENGTH = 32
export const USER_LAST_NAME_MIN_LENGTH = 3
export const USER_LAST_NAME_MAX_LENGTH = 32
export const ADMIN_FIRST_NAME_MIN_LENGTH = 3
export const ADMIN_FIRST_NAME_MAX_LENGTH = 32
export const ADMIN_LAST_NAME_MIN_LENGTH = 3
export const ADMIN_LAST_NAME_MAX_LENGTH = 32

export const PASSWORD_MIN_LENGTH = 6
export const PASSWORD_MAX_LENGTH = 16
export const EMAIL_MIN_LENGTH = 4
export const EMAIL_MAX_LENGTH = 100
export const TEXT_MIN_LENGTH = 32
export const TEXT_MAX_LENGTH = 8192
