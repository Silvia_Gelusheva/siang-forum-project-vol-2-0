import { useEffect, useState } from "react";

//IN PROGRESS

export default function Tarot() {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch("https://tarot-api.onrender.com/api/v1/cards/random?n=10")
      .then((response) => response.json())
      .then((response) => setData(response.cards));
  }, []);

  return (
    <div
      className=""   
    >
      <div>
     <div className="flex justify-center">
     {/* <div className="text-fuchsia-100 text-center text-2xl mb-8 w-[70%]">
        𝐓𝐡𝐞 𝐂𝐞𝐥𝐭𝐢𝐜 𝐂𝐫𝐨𝐬𝐬 𝐓𝐚𝐫𝐨𝐭 𝐒𝐩𝐫𝐞𝐚𝐝 𝐢𝐬 𝐚𝐥𝐫𝐞𝐚𝐝𝐲 𝐛𝐞𝐢𝐧𝐠 𝐮𝐬𝐞𝐝 𝐟𝐨𝐫 𝐨𝐯𝐞𝐫 𝐚 𝐜𝐞𝐧𝐭𝐮𝐫𝐲.
        𝐈𝐧 𝟏𝟗𝟏𝟏, 𝐀𝐫𝐭𝐡𝐮𝐫 𝐄𝐝𝐰𝐚𝐫𝐝 𝐖𝐚𝐢𝐭𝐞, 𝐜𝐨-𝐜𝐫𝐞𝐚𝐭𝐨𝐫 𝐨𝐟 𝐭𝐡𝐞 𝐰𝐞𝐥𝐥-𝐤𝐧𝐨𝐰𝐧 𝐑𝐢𝐝𝐞𝐫-𝐖𝐚𝐢𝐭𝐞
        𝐭𝐚𝐫𝐨𝐭 𝐝𝐞𝐜𝐤, 𝐩𝐮𝐛𝐥𝐢𝐬𝐡𝐞𝐝 𝐡𝐢𝐬 𝐯𝐞𝐫𝐬𝐢𝐨𝐧 𝐨𝐟 𝐭𝐡𝐞 𝐟𝐚𝐦𝐨𝐮𝐬 𝐭𝐚𝐫𝐨𝐭 𝐬𝐩𝐫𝐞𝐚𝐝.
        <a
          href="https://www.alittlesparkofjoy.com/celtic-cross-tarot-spread/"
          class="font-medium text-fuchsia-300 dark:text-blue-500 hover:underline target:_blank"
        >
          {" "}
          𝑹𝒆𝒂𝒅 𝒎𝒐𝒓𝒆.
        </a>{" "}
      </div> */}

     </div>
    <div className="flex justify-center">
        <div className="w-96 bg-gray-200 mt-12">
        <div className="">
          {/* <div className="text-3xl font-bold flex justify-center mb-5">
            ℭ𝔢𝔩𝔱𝔦𝔠 ℭ𝔯𝔬𝔰𝔰
          </div> */}
          {data?.map((d) => (
            <div className="flex justify-center font">{d.name}</div>
          ))}

          {/* <div className="card-actions justify-end">
            <button className="btn">Start</button>
          </div> */}
        </div>
      </div>
    </div>
     </div>
    </div>
  );
}
