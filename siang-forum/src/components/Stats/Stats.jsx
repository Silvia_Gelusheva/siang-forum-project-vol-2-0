import { useContext, useEffect, useState } from "react";

import { AppContext } from "../../context/app.context";
import { getAllPostsNum } from "../../services/post.services";
import { getAllUsers } from "../../services/members.services";

export default function Stats() {
  const { addToast } = useContext(AppContext);
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getAllPostsNum()
      .then((posts) => setPosts(posts))
      .catch((error) => addToast("error", error.message));
  }, []);

  useEffect(() => {
    getAllUsers()
      .then((users) => setUsers(users))
      .catch((error) => addToast("error", error.message));
  }, []);

  return (
    <div className="lg:w-full w-[80%]">
      <div className="bg-secondary stats stats-horizontal shadow w-full">
        <div className="stat flex flex-col justify-center items-center">
          <div className="stat-title">Posts:</div>
          <div className="stat-value">{posts.length}</div>
        </div>

        <div className="stat flex flex-col justify-center items-center">
          <div className="stat-title">Members:</div>
          <div className="stat-value">{users.length}</div>
        </div>
      </div>
    </div>
  );
}
