import * as Yup from "yup";

import { ErrorMessage, Field, Form, Formik } from "formik";

import { AppContext } from "../../context/app.context";
import SelectCategory from "./SelectCategory";
import { addPost } from "../../services/post.services";
import defaultAvatar from "/empty.jpg";
import { useContext } from "react";

export default function CreatePost({ toggle, setToggle }) {
  const { userData, addToast, postCount, setAppState, ...appState } =
    useContext(AppContext);

  const initialValues = {
    category: "",
    title: "",
    content: "",
  };
  const onSubmit = (values) => {
    submit(values);
  };

  const submit = async (values) => {
    try {
      await addPost(
        values.category,
        values.title,
        values.content,
        userData.username,
        userData.avatarUrl || defaultAvatar
      );
      setToggle(!toggle);
      addToast("success", "Post has been uploaded successfully!");
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const validationSchema = Yup.object({
    title: Yup.string().required("Title is required!"),
    category: Yup.string().required("Please, select category!"),
    content: Yup.string().required("Content is required!"),
  });

  const options = [
    { key: 0, value: "Select category" },
    { key: 1, value: "Random talks" },
    { key: 2, value: "Psychology" },
    { key: 3, value: "Philosophy and Mythology" },
    { key: 4, value: "Arts and crafts" },
    { key: 5, value: "Culture & Anthropology" },
    { key: 6, value: "History & Traveling" },
    { key: 7, value: "Other" },
  ];

  return (
    <div className="">
      <div className="p-10 flex-auto">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="overflow-hidden shadow-md sm:rounded-lg">
            <div className="p-1 bg-primary shadow-lg rounded-lg">
              <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
                options={options}
              >
                {() => (
                  <Form>
                    <div className="md:flex">
                      <div className="w-full px-6 py-8">
                        <div className="text-center">
                          <span className="text-2xl text-secondary font-bold">
                            New Post
                          </span>
                        </div>
                        <div className="mt-4 relative">
                          <SelectCategory options={options} />
                        </div>

                        <div className="mt-4 relative">
                          <Field
                            name="title"
                            placeholder="Title"
                            className="h-12 px-2 w-full rounded"
                          />
                        </div>
                        <div className="errors">
                          <ErrorMessage
                            name="title"
                            component="span"
                            className="text-sm font-bold text-error"
                          />
                        </div>

                        <div className="mt-4 relative">
                          <Field
                            name="content"
                            as="textarea"
                            placeholder="Content"
                            className="h-48 px-2 w-full rounded"
                          />
                        </div>
                        <div className="errors">
                          <ErrorMessage
                            name="content"
                            component="span"
                            className="text-sm font-bold text-error"
                          />
                        </div>

                        <div className="flex justify-end mt-4">
                          <button
                            className="btn border-none bg-black text-primary rounded hover:bg-accent hover:text-secondary"
                            type="submit"
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
