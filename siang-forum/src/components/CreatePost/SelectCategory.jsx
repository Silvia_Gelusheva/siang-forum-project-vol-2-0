import { ErrorMessage, Field } from "formik";

export default function SelectCategory({ options }) {
  return (
    <>
      <Field
        as="select"
        name="category"
        className="h-12 px-2 w-full bg-white text-info rounded"
      >
        {options.map((option) => {
          return (
            <option key={option.value} value={option.value}>
              {option.value}
            </option>
          );
        })}
      </Field>
      <div className="errors">
        <ErrorMessage
          name="category"
          component="span"
          className="text-sm font-bold text-error"
        />
      </div>
    </>
  );
}
