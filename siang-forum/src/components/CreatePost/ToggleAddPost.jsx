
export default function ToggleAddPost({ toggle, setToggle }) {
  return (
    <div>
      <button
        className="btn bg-primary border-none shadow-sm mr-2 text-info"
        onClick={() => setToggle(!toggle)}
      >
        Add Post
      </button>
    </div>
  );
}
