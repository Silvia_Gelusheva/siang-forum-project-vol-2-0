export default function Advertisement() {
  return (
    <div className="lg:w-full w-[80%]">     
      <div className="bg-inherit flex flex-col justify-center items-center w-full p-1">
        <img
          src="/advert1.jpg"
          alt="advert1"
        />
      </div>

      <div className="bg-inherit flex flex-col justify-center items-center w-full p-1 mt-2">
        <img
           src="/advert2.jpg"
          alt="advert2"
        />
      </div>
    </div>
  );
}
