import moment from "moment";

export const COLUMNS_MEMBERS = [
  {
    Header: "USERNAME",
    accessor: "username",
  },
  {
    Header: "Name",
    accessor: "firstname",
  },
  {
    Header: "Lastname",
    accessor: "lastname",
  },
  {
    Header: "Location",
    accessor: "livesIn",
    Cell: (props) => (
      <div className="flex lg:justify-center md:justify-start sm:justify-start">
        {props.value ? props.value : "-"}
      </div>
    ),
  },
  {
    Header: "Education",
    accessor: "education",
    Cell: (props) => (
      <div className="flex lg:justify-center md:justify-start sm:justify-start">
        {props.value ? props.value : "-"}
      </div>
    ),
  },
  {
    Header: "Work",
    accessor: "workplace",
    Cell: (props) => (
      <div className="flex lg:justify-center md:justify-start sm:justify-start">
        {props.value ? props.value : "-"}
      </div>
    ),
  },
  {
    Header: "Joined On",
    accessor: "registeredOn",
    Cell: (props) => moment(new Date(props.value)).format("L"),
  },
  // {
  //   Header: "Profile",
  //   accessor: "avatarUrl",
  //   Cell: () => {
  //     return (
  //       <button className=" text-info font-bold bg-gradient-to-br from-gray-500 to-gray-300 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-[#23395d] dark:focus:ring-[#608da2] shadow-sm shadow-[#608da2] dark:shadow-lg dark:shadow-fuchsia-800/80 rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
  //         <span className="">웃 유</span>
  //       </button>
  //     );
  //   },
  // },
];