import { useEffect, useMemo } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";

import { AppContext } from "../../context/app.context";
import { COLUMNS_MEMBERS } from "./ColumnsMembers";
import { getAllMembers } from "../../services/members.services";
import { useContext } from "react";

export const Pagination = ({ users }) => {
 
  const columns = useMemo(() => COLUMNS_MEMBERS, []);
  const data = useMemo(() => users, [users]);

  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: { pageSize: 5 },
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    pageOptions,
    state,
    setGlobalFilter,
    gotoPage,
    pageCount,
    setPageSize,
    prepareRow,
  } = tableInstance;
  const { pageIndex, pageSize, globalFilter } = state;

  return (
    <div className="flex flex-col lg:flex-row w-[80%] lg:w-full justify-center text-sm px-5 py-2 mb-8 text-center">
    <span className=" bg-primary text-secondary font-bold text-sm px-5 py-2.5 text-center mr-2 mb-2">
      Page{" "}
      <strong>
        {pageIndex + 1} of {pageOptions.length}
      </strong>{" "}
    </span>
    <span className="bg-primary text-secondary font-bold text-sm px-5 py-2.5 text-center mr-2 mb-2">
      Go to page:{" "}
      <input
        type="number"
        defaultValue={pageIndex + 1}
        onChange={(e) => {
          const pageNumber = e.target.value
            ? Number(e.target.value) - 1
            : 0;
          gotoPage(pageNumber);
        }}
        style={{ width: "50px" }}
      />
    </span>
    <select
      className="text-secondary font-bold bg-primary text-sm px-5 py-2.5 text-center mr-2 mb-2"
      value={pageSize}
      onChange={(e) => setPageSize(Number(e.target.value))}
    >
      {[3, 5, 8].map((pageSize) => (
        <option key={pageSize} value={pageSize}>
          {" "}
          Show {pageSize}
        </option>
      ))}
    </select>
    <button
      className="text-secondary font-bold bg-primary text-sm px-5 py-2.5 text-center mr-2 mb-2"
      onClick={() => gotoPage(0)}
      disabled={!canPreviousPage}
    >
      {" "}
      {"<<"}
    </button>
    <button
      className="text-secondary font-bold bg-primary text-sm px-5 py-2.5 text-center mr-2 mb-2"
      onClick={() => previousPage()}
      disabled={!canPreviousPage}
    >
      Prev
    </button>

    <button
      className="text-secondary font-bold bg-primary text-sm px-5 py-2.5 text-center mr-2 mb-2"
      onClick={() => nextPage()}
      disabled={!canNextPage}
    >
      Next
    </button>
    <button
      className="text-secondary font-bold bg-primary text-sm px-5 py-2.5 text-center mr-2 mb-2"
      onClick={() => gotoPage(pageCount - 1)}
      disabled={!canNextPage}
    >
      {" "}
      {">>"}
    </button>
  </div>
  );
};
