import {
  TbZodiacAquarius,
  TbZodiacAries,
  TbZodiacCancer,
  TbZodiacCapricorn,
  TbZodiacGemini,
  TbZodiacLeo,
  TbZodiacLibra,
  TbZodiacPisces,
  TbZodiacSagittarius,
  TbZodiacScorpio,
  TbZodiacTaurus,
  TbZodiacVirgo,
} from "react-icons/tb";
import { useEffect, useState } from "react";

import { getSign } from "../../services/fetch.services";

export default function SelectedSign({ onSignSelected }) {
  const [signs, setSigns] = useState([]);

  useEffect(() => {
    getSign().then(setSigns);
  }, []);

  return (
    <div className="grid grid-cols-2 lg:grid-cols-4 mb-32 gap-6">
      {signs.map((sign) => {
        switch (sign) {
          case "aries":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacAries
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );

          case "taurus":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacTaurus
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "gemini":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacGemini
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "cancer":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacCancer
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "leo":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacLeo size={80} onClick={() => onSignSelected(sign)} />
                <div>{sign}</div>
              </div>
            );
          case "virgo":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacVirgo
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "libra":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacLibra
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "scorpio":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacScorpio
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />

                <div> {sign}</div>
              </div>
            );
          case "sagittarius":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacSagittarius
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "capricorn":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacCapricorn
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "aquarius":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacAquarius
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
          case "pisces":
            return (
              <div key={sign} className="flex flex-col justify-center items-center bg-secondary shadow-xl lg:p-8 p-12">
                <TbZodiacPisces
                  size={80}
                  onClick={() => onSignSelected(sign)}
                />
                <div>{sign}</div>
              </div>
            );
        }
      })}
    </div>
  );
}
