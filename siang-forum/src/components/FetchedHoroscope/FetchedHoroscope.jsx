import { useEffect, useState } from "react";

import { getHoroscope } from "../../services/fetch.services";

export default function FetchedHoroscope({ sign, time }) {
  const [horoscope, setHoroscope] = useState([]);
  useEffect(() => {
    getHoroscope(sign, time).then(setHoroscope);
  }, []);

  return (
    <div className="flex flex-col bg-slate-100 lg:w-[70%] shadow-xl p-5">
      <div className="uppercase text-secondary font-bold mb-2">{sign}</div>
      {horoscope}
    </div>
  );
}
