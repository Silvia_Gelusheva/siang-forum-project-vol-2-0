export default function SelectTimeFrame({ onTimeFrameSelected }) {
  return (
    <div className="lg:grid grid-cols-3 gap-2">
      {["yesterday", "today", "tomorrow"].map((timeFrame) => (
        <div className="w-full flex flex-col lg:flex-row" key={timeFrame}>
          <div className="divider divider-horizontal"></div>
          <div
            onClick={() => onTimeFrameSelected(timeFrame)}
            className="grid h-40 w-80 flex-grow text-xl text-primary border-2 border-gray-300 p-6 cursor-pointer rounded-box place-items-center uppercase"
            style={{
              backgroundImage:
                "url('https://images.ctfassets.net/cnu0m8re1exe/n05SaIlQxJKXj00M87xa1/10a199a0d83970c4fbf225c078b791fb/shutterstock_272539379.jpg?fm=jpg&fl=progressive&w=660&h=433&fit=fill')",
            }}
          >
            {" "}
            {timeFrame}
          </div>
          <div className="divider divider-horizontal"></div>
        </div>
      ))}
    </div>
  );
}
