import { getDownloadURL, ref, uploadBytes } from "firebase/storage";

import { AppContext } from "../../context/app.context";
import { storage } from "../../firebase/config";
import { updateUserProfilePic } from "../../services/users.services";
import { useContext } from "react";

export default function UploadAvatar( {username}) {
    const { user, userData, setAppState, addToast } = useContext(AppContext);
  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files[0];
    if (!file) return addToast("error", "Please select file!");
    const picture = ref(storage, `images/${username}/avatar`);
    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePic(username, url).then(() => {
            setAppState({
              user,
              userData: {
                ...userData,
                avatarUrl: url,
              },
            });
          });
        });
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <div className="UploadAvatar">
      <form onSubmit={uploadPicture}>
        <input
          type="file"
          name="file"
          accept=".jpeg, .jpg, .png"
          className="text-sm text-grey-500
         file:mr-5 file:py-3 file:px-10
         file:rounded-full file:border-0
         file:text-md file:font-semibold  file:text-white
         file:bg-warning  file:to-[#3BB78F]
         hover:file:cursor-pointer hover:file:opacity-80"
        />
        <button
          type="submit"
          className=" text-white font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center "
          style={{ transition: "all .15s ease" }}
        >
          {" "}
          Update AVATAR
        </button>
      </form>
    </div>
  );
}
