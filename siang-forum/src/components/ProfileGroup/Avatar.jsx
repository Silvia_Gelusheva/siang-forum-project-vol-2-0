import defaultAvatar from "/empty.jpg";
export default function Avatar({ currentUser }) {
  return (
    <div className="Avatar">
      <img
        className="shadow-xl rounded-full w-38 h-38 align-middle border-none absolute -m-16 -ml-20"
        style={{ maxWidth: "150px" }}
        src={currentUser?.avatarUrl || defaultAvatar}
        alt="profile"
      />
    </div>
  );
}
