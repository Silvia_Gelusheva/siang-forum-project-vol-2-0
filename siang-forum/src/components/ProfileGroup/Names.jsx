export default function Names({ state }) {
  return (
    <div className="Names">
      <div className="text-center mt-4 flex gap-3 justify-center">
        <div className="inline text-neutral font-bold text-3xl">
          {state?.profile?.firstname}
        </div>

        <div className="inline text-neutral font-bold text-3xl ">
          {state?.profile?.lastname}
        </div>
      </div>
    </div>
  );
}
