// import { VscSymbolNamespace } from "react-icons/vsc";
import { MdDateRange, MdWork } from "react-icons/md";

import { AiTwotoneHome } from "react-icons/ai";
import { BsFillPersonFill } from "react-icons/bs";
import { GiHeartBeats } from "react-icons/gi";
import { GoLocation } from "react-icons/go";
import { IoSchoolSharp } from "react-icons/io5";

GoLocation;
export default function UserInfo({ currentUser }) {
  return (
    <div className="my-2 px-20 mb-10 bg-inherit border border-solid border-gray-300 shadow-lg">
      <div className="bg-inherit rounded overflow-hidden ">
        <div className="text-center text-2xl mt-2 flex gap-1 justify-center">
          <div className="mt-2"></div>
        </div>
        <div className="mb-5">
          <div className="bg-inherit rounded overflow-hidden flex flex-col items-center">
            <div className="border-b">
              <div className="px-4 py-2 hover:bg-accent flex">
                <div className="text-neutral">
                  <BsFillPersonFill />
                </div>
                <div className="pl-3">
                  <p className="text-sm font-medium text-neutral leading-none">
                    Username
                  </p>
                  <p className="text-md font-bold text-blue-800">
                    {" "}
                    {currentUser?.username}
                  </p>
                </div>
              </div>
              <div className="px-4 py-2 hover:bg-accent flex">
                <div className="text-neutral">
                  <MdDateRange />
                </div>
                <div className="pl-3">
                  <p className="text-sm font-medium text-neutral leading-none">
                    Joined On
                  </p>
                  <p className="text-md font-bold text-warning">
                    <span className="">
                      {`  ${new Date(
                        currentUser?.registeredOn
                      ).toLocaleDateString("en-GB")}`}
                    </span>
                  </p>
                </div>
              </div>
              {currentUser?.bornIn && (
                <div className="px-4 py-2 hover:bg-accent flex">
                  <div className="text-neutral">
                    <AiTwotoneHome />
                  </div>

                  <div className="pl-3">
                    <p className="text-sm font-medium text-neutral leading-none">
                      Hometown
                    </p>
                    <p className="font-bold text-orange-700">
                      {currentUser?.bornIn}
                    </p>
                  </div>
                </div>
              )}
              {currentUser?.livesIn && (
                <div className="px-4 py-2 hover:bg-accent flex">
                  <div className="text-neutral">
                    <GoLocation />
                  </div>

                  <div className="pl-3">
                    <p className="text-sm font-medium text-neutral leading-none">
                      Location
                    </p>
                    <p className="font-bold text-orange-800">
                      {currentUser?.livesIn}
                    </p>
                  </div>
                </div>
              )}
              {currentUser?.education && (
                <div className="px-4 py-2 hover:bg-accent flex">
                  <div className="">
                    <IoSchoolSharp />
                  </div>
                  <div className="pl-3">
                    <p className="text-sm font-medium text-neutral leading-none">
                      Education
                    </p>
                    <p className="font-bold text-green-600">
                      {currentUser?.education}
                    </p>
                  </div>
                </div>
              )}
              {currentUser?.workplace && (
                <div className="px-4 py-2 hover:bg-accent flex">
                  <div className="">
                    <MdWork />
                  </div>
                  <div className="pl-3">
                    <p className="text-sm font-medium text-neutral leading-none">
                      Occupation
                    </p>
                    <p className="font-bold text-green-700">
                      {currentUser?.workplace}
                    </p>
                  </div>
                </div>
              )}
              {currentUser?.hobbies && (
                <div className="px-4 py-2 hover:bg-accent flex">
                  <div className="">
                    <GiHeartBeats />
                  </div>
                  <div className="pl-3">
                    <p className="text-sm font-medium  leading-none">Hobbies</p>
                    <p className="font-bold text-neutral ">
                      {currentUser?.hobbies}
                    </p>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
