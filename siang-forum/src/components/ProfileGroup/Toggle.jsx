import { AiOutlineEdit } from "react-icons/ai";
import { VscSaveAs } from "react-icons/vsc";

export default function Toggle({ toggle, setToggle }) {

  return (
    <div className="Toggle">
      <div className="text-center flex justify-center">
        <button
          className="text-gray-500 font-bold bg-accent hover:bg-gradient-to-br rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
          onClick={() => setToggle(!toggle)}
        >
          {!toggle ? (
            <div className="flex flex-row gap-2 ">
              Edit Profile <AiOutlineEdit style={{ margin: "1px" }} size={20} />{" "}
            </div>
          ) : (
            <div className="flex flex-row gap-2 ">
              Save <VscSaveAs style={{ margin: "1px" }} size={20} />{" "}
            </div>
          )}
        </button>
      </div>
    </div>
  );
}
