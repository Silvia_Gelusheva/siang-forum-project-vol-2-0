export default function Statistics({ currentUser }) {
  console.log((currentUser))
  return (
    <div className="flex justify-center py-4 lg:pt-4 pt-8">
      <div className="mr-4 p-3 text-center">
        <span className="text-xl font-bold block uppercase tracking-wide text-neutral">
          {Object.keys(currentUser?.posts || {}).length}
        </span>
        <span className="text-sm font-semibold text-neutral">Posts</span>
      </div>
      <div className="mr-4 p-3 text-center">
        <span className="text-xl font-bold block uppercase tracking-wide text-neutral">
          {Object.keys(currentUser?.comments || {}).length}
        </span>
        <span className="text-sm font-semibold  text-neutral">Comments</span>
      </div>
      <div className="lg:mr-4 p-3 text-center">
        <span className="text-xl font-bold block uppercase tracking-wide text-neutral">
          {Object.keys(currentUser?.likedPosts || {}).length}
        </span>
        <span className="text-sm  font-semibold text-neutral">Likes</span>
      </div>
    </div>
  );
}
