import { blockUser, unblockUser } from "../../services/users.services";

export default function BlockUser({ currentUser }) {
  return (
    <div className="Toggle">
      {currentUser?.isActive && (
        <div className="">  
          <button
            className="text-gray-200 font-bold bg-gradient-to-br from-red-800 to-red-400 hover:bg-gradient-to-r rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
            onClick={() => blockUser(currentUser?.username)}
          >
            Block
          </button>
        </div>
      )}
      {!currentUser?.isActive && (
        <div className="">
          <button
            className="text-gray-200 font-bold bg-gradient-to-br from-green-800 to-green-400 hover:bg-gradient-to-r  rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
            onClick={() => unblockUser(currentUser?.username)}
          >
            Unblock
          </button>
        </div>
      )}
    </div>
  );
}
