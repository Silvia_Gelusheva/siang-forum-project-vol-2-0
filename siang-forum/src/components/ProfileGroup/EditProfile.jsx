import {
  editBornIn,
  editEducation,
  editFirstName,
  editHobbies,
  editLastName,
  editLivesIn,
  editWorkPlace,
} from "../../services/users.services";

import { AiOutlineEdit } from "react-icons/ai";
import { AppContext } from "../../context/app.context";
import UploadAvatar from "./UploadAvatar";
import { useContext } from "react";

export default function EditProfile({ state, setState, currentUserName }) {
  const { addToast } = useContext(AppContext);

  const saveFirstName = async (e) => {
    e.preventDefault();
    if (
      state?.firstNameText === "" ||
      state?.firstNameText === state?.profile?.firstname
    )
      return setState({ ...state, showEditFirstName: false });
    try {
      await editFirstName(currentUserName, state.firstNameText);

      setState({
        ...state,
        showEditFirstName: false,
        profile: {
          ...state.profile,
          firstname: state.firstNameText,
        },
      });
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const saveLastName = async (e) => {
    e.preventDefault();
    if (
      state?.lastNameText === "" ||
      state?.lastNameText === state?.profile?.lastname
    )
      return setState({ ...state, showEditLastName: false });

    try {
      await editLastName(currentUserName, state.lastNameText);

      setState({
        ...state,
        showEditLastName: false,
        profile: {
          ...state.profile,
          lastname: state.lastNameText,
        },
      });
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const saveBornIn = async (e) => {
    e.preventDefault();
    if (state?.bornInText === "" || state?.bornInText === state?.profile.bornIn)
      return setState({ ...state, showEditBornIn: false });
    try {
      await editBornIn(currentUserName, state.bornInText);
      setState({
        ...state,
        showEditBornIn: false,
        profile: {
          ...state.profile,
          bornIn: state.bornInText,
        },
      });
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const saveLivesIn = async (e) => {
    e.preventDefault();
    if (
      state?.livesInText === "" ||
      state?.livesInText === state?.profile?.livesIn
    )
      return setState({ ...state, showEditLivesIn: false });
    try {
      await editLivesIn(currentUserName, state.livesInText);
      setState({
        ...state,
        showEditLivesIn: false,
        profile: {
          ...state.profile,
          livesIn: state.livesInText,
        },
      });
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const saveEducation = async (e) => {
    e.preventDefault();
    if (
      state?.educationText === "" ||
      state?.educationText === state?.profile?.education
    )
      return setState({ ...state, showEditEducation: false });

    try {
      await editEducation(currentUserName, state.educationText);
      setState({
        ...state,
        showEditEducation: false,
        profile: {
          ...state.profile,
          education: state.educationText,
        },
      });
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const saveWorkPlace = async (e) => {
    e.preventDefault();
    if (
      state?.workplaceText === "" ||
      state?.workplaceText === state?.profile?.workplace
    )
      return setState({ ...state, showEditWorkplace: false });

    try {
      await editWorkPlace(currentUserName, state.workplaceText);
      setState({
        ...state,
        showEditWorkplace: false,
        profile: {
          ...state.profile,
          workplace: state.workplaceText,
        },
      });
    } catch (error) {
      addToast("error", error.message);
    }
  };

  const saveHobbies = async (e) => {
    e.preventDefault();
    if (
      state?.hobbiesText === "" ||
      state?.hobbiesText === state?.profile?.hobbies
    )
      return setState({ ...state, showEditHobbies: false });

    try {
      await editHobbies(currentUserName, state.hobbiesText);
      setState({
        ...state,
        showEditHobbies: false,
        profile: {
          ...state.profile,
          hobbies: state.hobbiesText,
        },
      });
    } catch (error) {
      addToast("error", error.message);
    }
  };

  return (
      <div className="Bio my-12">
      <body className="bg-inherit flex justify-center items-center">
        <div className="w-2/3 flex flex-col bg-white shadow-lg rounded-lg overflow-hidden">
          <div className="bg-secondary text-gray-700 text-lg px-6 pb-6 flex justify-center overflow-hidden">
                   <div className="flex flex-col bg-secondary rounded-lg overflow-hidden">
                   <UploadAvatar username={currentUserName} />
                   </div>
     
          </div>
             <div className="px-6 py-4 border-t border-gray-200">
            <p>Name</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditFirstName && (
                  <input
                    className="input w-full text-primary"
                    value={state?.firstNameText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        firstNameText: e.target.value,
                      })
                    }
                    type="text"
                    name="bornIn"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditFirstName: !state?.showEditFirstName,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditFirstName && state?.profile?.firstname}
                    </div>
                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditFirstName ? "" : "hidden"
                  }`}
                  onClick={saveFirstName}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Last name</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditLastName && (
                  <input
                    className="input w-full text-primary"
                    value={state?.lastNameText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        lastNameText: e.target.value,
                      })
                    }
                    type="text"
                    name="bornIn"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditLastName: !state?.showEditLastName,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditLastName && state?.profile?.lastname}
                    </div>
                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditLastName ? "" : "hidden"
                  }`}
                  onClick={saveLastName}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Hometown</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditBornIn && (
                  <input
                    className="input w-full text-primary"
                    value={state?.bornInText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        bornInText: e.target.value,
                      })
                    }
                    type="text"
                    name="bornIn"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditBornIn: !state?.showEditBornIn,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditBornIn && state?.profile?.bornIn}
                    </div>

                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditBornIn ? "" : "hidden"
                  }`}
                  onClick={saveBornIn}
                >
                  Save
                </button>

            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Location</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditLivesIn && (
                  <input
                    className="input w-full text-primary"
                    value={state?.livesInText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        livesInText: e.target.value,
                      })
                    }
                    type="text"
                    name="livesIn"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditLivesIn: !state?.showEditLivesIn,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditLivesIn && state?.profile?.livesIn}
                    </div>

                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditLivesIn ? "" : "hidden"
                  }`}
                  onClick={saveLivesIn}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Education</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditEducation && (
                  <input
                    className="input w-full text-primary"
                    value={state?.educationText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        educationText: e.target.value,
                      })
                    }
                    type="text"
                    name="education"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditEducation: !state?.showEditEducation,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditEducation && state?.profile?.education}
                    </div>

                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditEducation ? "" : "hidden"
                  }`}
                  onClick={saveEducation}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Occupation</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditWorkplace && (
                  <input
                    className="input w-full text-primary"
                    value={state?.workplaceText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        workplaceText: e.target.value,
                      })
                    }
                    type="text"
                    name="workplace"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditWorkplace: !state?.showEditWorkplace,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditWorkplace && state?.profile?.workplace}
                    </div>

                    <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditWorkplace ? "" : "hidden"
                  }`}
                  onClick={saveWorkPlace}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="px-6 py-4 border-t border-gray-200">
            <p>Hobbies</p>
            <div className="border rounded-lg p-4 bg-secondary">
            {state?.showEditHobbies && (
                  <input
                    className="input w-full text-primary"
                    value={state?.hobbiesText}
                    onChange={(e) =>
                      setState({
                        ...state,
                        hobbiesText: e.target.value,
                      })
                    }
                    type="text"
                    name="hobbies"
                  />
                )}
                <span
                  className=""
                  onClick={() =>
                    setState({
                      ...state,
                      showEditHobbies: !state?.showEditHobbies,
                    })
                  }
                >
                  <div className="flex justify-start">
                    <div className="">
                      {!state?.showEditHobbies && state?.profile?.hobbies}
                    </div>

                   <AiOutlineEdit style={{paddingLeft:'8px'}} size={24}/>
                  </div>
                </span>
                <button
                  className={`text-gray-200 font-bold bg-warning rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 ${
                    state?.showEditHobbies ? "" : "hidden"
                  }`}
                  onClick={saveHobbies}
                >
                  Save
                </button>
            </div>
          </div>

          <div className="bg-secondary px-6 py-4">
            <div className="flex items-center pt-3">
             <div className="ml-4"></div>
            </div>
          </div>
        </div>
      </body>
    </div>
    )

}
