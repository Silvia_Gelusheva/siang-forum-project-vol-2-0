import { AiOutlineDelete } from "react-icons/ai";
import { AppContext } from "../../context/app.context";
import { BiMessage } from "react-icons/bi";
import LikePost from "./LikePost";
import defaultAvatar from "/empty.jpg";
import { useContext } from "react";

export default function CurrentPost({
  state,
  setState,
  saveTitle,
  saveContent,
  toggle,
  toggleLike,
  setToggle,
  author,
  avatarUrl,
  addedOn,
  removePost,
  postId,
}) {
  const { userData } = useContext(AppContext);
  return (
    <div className="flex flex-row justify-center items-center bg-gray-100 mt-8 mx-10 rounded-lg px-4">
      <div className="flex flex-col justify-center items-center bg-white w-[12%] p-8 rounded-lg">
        <div className="flex rounded-full justify-center items-center w-12 h-12">
          <img
            className="w-10 h-10 rounded-full"
            src={avatarUrl || defaultAvatar}
            alt="default_avatar"
          />
        </div>
        <div className="flex flex-col justify-center items-center">
          <p>{author}</p>
          <p>{addedOn}</p>
        </div>
      </div>

      <div className="flex flex-col bg-gray-100 m-1 rounded-lg p-4 w-[90%]">
        <div
          className="flex font-semibold text-xl cursor-pointer "
          onClick={() =>
            setState({
              ...state,
              showEditTitle: !state.showEditTitle,
            })
          }
        >
          {!state.showEditTitle && state.post?.title}
        </div>
        {state?.showEditTitle && (
          <input
            className="input w-full text-gray-700 border-black"
            value={state.titleText}
            onChange={(e) => setState({ ...state, titleText: e.target.value })}
            type="text"
            name="name"
          />
        )}
        <div className="flex my-2 justify-center items-center">
          <button
            className={`btn btn-sm bg-green-300 text-gray-700 border-none ${
              state.showEditTitle ? "" : "hidden"
            }`}
            onClick={saveTitle}
          >
            Save
          </button>
        </div>

        <div
          className="cursor-pointer"
          onClick={() =>
            setState({
              ...state,
              showEditContent: !state.showEditContent,
            })
          }
        >
          {!state.showEditContent && state?.post?.content}
        </div>

        {state?.showEditContent && (
          <textarea
            className="input h-32 w-full p-4 mt-2 text-gray-700 border border-black"
            value={state.contentText}
            onChange={(e) =>
              setState({ ...state, contentText: e.target.value })
            }
            type="text"
          ></textarea>
        )}
        <div className="flex my-2 justify-center items-center">
          <button
            className={`btn btn-sm bg-green-300 text-gray-700 border-none ${
              state.showEditContent ? "" : "hidden"
            }`}
            onClick={saveContent}
          >
            Save
          </button>
        </div>

        {/* like, comment, delete */}
        <div className="flex flex-row gap-9 justify-center mt-4 bg-white rounded-lg p-2">
          <LikePost toggleLike={toggleLike} state={state} />
          <div
            className="flex flex-row gap-1 cursor-pointer"
            onClick={() => setToggle(!toggle)}
          >
            {" "}
            <BiMessage size={20} /> Comment
          </div>

          {userData?.username === author || userData?.role === 2 ? (
            <div
              className="flex flex-row gap-1 cursor-pointer"
              onClick={() => removePost(postId, userData?.username)}
            >
              {" "}
              <AiOutlineDelete size={20} /> Delete
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}
