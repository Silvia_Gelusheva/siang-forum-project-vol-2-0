import { AppContext } from "../../context/app.context";
import { RiDeleteBinLine } from "react-icons/ri";
import { useContext } from "react";
export default function DeletePost({ removePost, state, postId }) {
  const { userData } = useContext(AppContext);

  return (
    <div>
      {state?.post?.author === userData?.username || userData?.role === 2 ? (
        <div className="">
          <button
            className="btn btn-sm text-info font-bold bg-gradient-to-br from-gray-400 to-gray-300 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-[#23395d] dark:focus:ring-[#608da2] shadow-sm shadow-[#608da2] dark:shadow-lg dark:shadow-fuchsia-800/80 rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
            onClick={() => removePost(postId, userData?.username)}
          >
            <RiDeleteBinLine />
          </button>
        </div>
      ) : null}
    </div>
  );
}
