import { AppContext } from "../../context/app.context";
import { MdOutlineArrowForwardIos } from "react-icons/md";
import { useContext } from "react";

export default function AddComment({ addPostComment, state, setState }) {
  const { userData } = useContext(AppContext);

  return (
    <div className="flex flex-row justify-center items-center bg-secondary mx-10 mt-4 rounded-lg px-4">
    <div className="flex flex-col bg-secondary rounded-lg w-[90%] justify-center">
      {userData?.username && (
        <div className="flex bg-secondary mt-4">
          <div className="bg-secondary w-full">
            <div className="">
              <div className="flex w-full md:inline-flex ">
                <textarea
                  value={state.commentText}
                  onChange={(e) =>
                    setState({ ...state, commentText: e.target.value })
                  }
                  type="textarea"
                  placeholder="add comment"
                  className="h-18 w-full m-5 border border-gray-400 rounded-md text-primary"
                ></textarea>
              </div>

              <div className="flex justify-end mr-5">
                <button
                  className="btn btn-sm bg-inherit text-gray-700 hover:text-secondary mb-2 cursor-pointer"
                  onClick={addPostComment}
                >
                  <MdOutlineArrowForwardIos />
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
    </div>
  );
}
