import { AiOutlineDelete, AiTwotoneLike } from "react-icons/ai";
import { BiDislike, BiLike, BiMessage } from "react-icons/bi";

import { AppContext } from "../../context/app.context";
import defaultAvatar from "/empty.jpg";
import { useContext } from "react";

export default function Comment({
  author,
  avatarUrl,
  commentId,
  comment,
  addedOn,
  removeComment,
  postId,
}) {
  const { userData } = useContext(AppContext);

  return (
    <>
      <div className="flex flex-row justify-center items-center bg-gray-100 mx-10 rounded-lg p-4 my-2">
        <div className="flex flex-col justify-center items-center bg-white w-[12%] p-8 rounded-lg">
          <div className="flex rounded-full justify-center items-center w-12 h-12">
            <img
              className="w-10 h-10 rounded-full"
              src={avatarUrl || defaultAvatar}
              alt="default_avatar"
            />
          </div>
          <div className="flex flex-col justify-center items-center">
            <p>{author}</p>
            <p>{new Date(addedOn).toLocaleDateString("en-GB")}</p>
          </div>
        </div>

        <div className="flex flex-col bg-gray-100 m-1 rounded-lg px-4 w-[90%]">
          <p> {comment}</p>{" "}
          <div className="flex justify-end items-end">
            <div className="flex flex-row justify-end gap-4 w-[5%] mt-4 bg-white rounded-lg p-2">
              <div>
                {userData?.username && (
                  <div
                  // onClick={toggleLike}
                  >
                    {userData ? (
                      <div className="flex flex-row gap-1 cursor-pointer">
                        {" "}
                        <BiLike size={20} />
                      </div>
                    ) : (
                      <div className="flex flex-row gap-1 cursor-pointer">
                        {" "}
                        <AiTwotoneLike size={20} />
                      </div>
                    )}
                  </div>
                )}
              </div>

              {userData?.username === author || userData?.role === 2 ? (
                <div
                  className="flex flex-row gap-1 cursor-pointer"
                 onClick={() => removeComment(commentId, userData?.username, postId)}
                >
                  {" "}
                  <AiOutlineDelete size={20} />
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
