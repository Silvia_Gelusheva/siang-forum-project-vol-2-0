import { AiTwotoneLike } from "react-icons/ai";
import { AppContext } from "../../context/app.context";
import { BiLike } from "react-icons/bi";
import { useContext } from "react";

export default function LikePost({ toggleLike, state }) {
  const { userData } = useContext(AppContext);

  return (
    <div>
    {userData?.username && (
      <div onClick={toggleLike}>
        {!userData?.likedPostsIds.includes(state?.post?.id) ? (
          <div className="flex flex-row gap-1 cursor-pointer">
            {" "}
            <BiLike size={20} />
            Like
          </div>
        ) : (
          <div className="flex flex-row gap-1 cursor-pointer">
            {" "}
            <AiTwotoneLike size={20} />
            Liked
          </div>
        )}
      </div>
    )}
  </div>
  );
}
