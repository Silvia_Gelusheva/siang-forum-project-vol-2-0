import { useEffect, useState } from "react";

import defaultAvatar from "/empty.jpg";
import { getAllMembers } from "../../services/members.services";
import { useNavigate } from "react-router-dom";

export default function Admins() {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    getAllMembers()
      .then(setUsers)
      .then(console.log(users))
      .catch((e) => console.log(e.message));
  }, []);
  return (
    <div className="lg:w-full w-[98%]">
      <div className="bg-accent flex flex-col justify-center items-center mt-8 w-full rounded-lg">
        {/* <div className="flex justify-start items-start"> */}
          <h2 className="font-bold text-neutral">ADMINS</h2>
        </div>
        {users
          .filter((user) => user.role === 2)
          .map((user) => (
            <div
              key={user.username}
              className="bg-secondary m-1 w-[98%] rounded-lg p-2 flex flex-row items-center justify-start cursor-pointer hover:bg-gray-200"
              onClick={() => navigate(`/members/profile/${user.username}`)}
            >
              <div className="flex rounded-full">
                {user.avatarUrl ? (
                  <img
                    className="w-10 h-10 rounded-full"
                    src={user.avatarUrl}
                    alt="avatar"
                  />
                ) : (
                  <img
                    className="w-10 h-10 rounded-full"
                    src={defaultAvatar}
                    alt="default_avatar"
                  />
                )}
              </div>
              <div className="ml-20">{user.username}</div>
            </div>
          ))}
      {/* </div> */}
    </div>
  );
}
