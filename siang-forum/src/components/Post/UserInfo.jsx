import defaultAvatar from "/empty.jpg";

export default function UserInfo({ currentUser, post }) {
  return (
    <>
      <div className="mt-3 flex items-center justify-center font-sans">
        <div className="shrink-0 ">
          {currentUser.avatarUrl ? (
            <img
              className="w-10 h-10 rounded-full"
              src={currentUser.avatarUrl}
              alt="avatar"
            />
          ) : (
            <img
              className="w-10 h-10 rounded-full"
              src={defaultAvatar}
              alt="default_avatar"
            />
          )}
        </div>

        <div className="ml-3">
          <p className="text-sm font-medium text-skin-inverted">
            {post.author}
          </p>

          <div className="flex space-x-1 text-xs text-skin-muted ">
            <time>
              Member since:{" "}
              {`  ${new Date(currentUser?.registeredOn).toLocaleDateString(
                "en-GB"
              )}`}
            </time>
          </div>
        </div>
      </div>
    </>
  );
}
