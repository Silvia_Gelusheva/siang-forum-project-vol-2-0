import { useContext, useEffect, useState } from "react";

import { AppContext } from "../../context/app.context";
import PostInfo from "./PostInfo";
import UserInfo from "./UserInfo";
import { getUser } from "../../services/users.services";

export default function Post({ post, removePost }) {
  const { userData } = useContext(AppContext);
  const [currentUser, setCurrentUser] = useState({});

  useEffect(() => {
    getUser(post.author).then(setCurrentUser);
  }, [post]);

  return (
    <div className="container mx-auto p-4 mt-3 bg-secondary ">
      <div>
        <div className="space-y-4 lg:grid lg:grid-cols-3 lg:items-center lg:gap-6 lg:space-y-0">
          <UserInfo currentUser={currentUser} post={post} />

          <div className="sm:col-span-2">
            <div className="flex items-start space-x-3">
              <div className="flex lg:items-start items-center space-x-2 flex-col w-full ">
                <PostInfo
                  post={post}
                  removePost={removePost}
                  userData={userData}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
