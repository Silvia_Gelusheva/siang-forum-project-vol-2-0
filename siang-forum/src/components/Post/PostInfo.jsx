import { BiLike, BiMessage } from "react-icons/bi";

import { AiTwotoneDelete } from "react-icons/ai";
import { Link } from "react-router-dom";

export default function PostInfo({ post, removePost, userData }) {
  return (
    <>
      <div className="flex flex-row items-center mb-2">
        <p className="text-sm font-semibold text-skin-base leading-5">
          <Link to={`/posts/${post.id}`}>{post.title} </Link>
        </p>
        {post.author === userData?.username || userData?.role === 2 ? (
          <div
            className=" flex flex-row ml-3 cursor-pointer"
            onClick={() => removePost(post.id, userData?.username)}
          >
            {" "}
            <AiTwotoneDelete size={16} />
          </div>
        ) : null}
      </div>

      <div className="flex flex-row text-sm">
        <p className=" italic"> category:</p>{" "}
        <p className=" ml-1 font-semibold">{post.category}</p>
      </div>
      <div className="flex flex-row text-sm">
        <p className=" italic"> added on:</p>{" "}
        <p className="ml-1 font-semibold">
          {new Date(post?.addedOn).toLocaleDateString("en-GB")}
        </p>
      </div>
      <div className="flex flex-row gap-3 mr-10 text-black mt-2">
        <div className="flex flex-row items-center  gap-1 ">
          {" "}
          {post.likedBy ? Object.keys(post.likedBy).length : 0}{" "}
          <BiLike size={16} />
        </div>{" "}
        <div className="flex flex-row gap-1 items-center">
          {" "}
          {post.comments ? Object.keys(post.comments).length : 0}{" "}
          <BiMessage size={16} />
        </div>
      </div>
    </>
  );
}
