import { BsMoonStars, BsMoonStarsFill, BsSun, BsSunFill } from "react-icons/bs";
import { useEffect, useState } from "react";

export default function Title() {
  const [theme, setTheme] = useState(
    localStorage.getItem("theme") ? localStorage.getItem("theme") : "garden"
  );

  const handleToggle = (e) => {
    if (e.target.checked) {
      setTheme("coffee");
    } else {
      setTheme("garden");
    }
  };
  useEffect(() => {
    localStorage.setItem("theme", theme);
    const localTheme = localStorage.getItem("theme");
    document.querySelector("html").setAttribute("data-theme", localTheme);
  }, [theme]);

  return (
    <div className="bg-black w-full h-20 flex flex-row justify-between">
      <p className="flex items-center text-accent text-extrabold lg:text-5xl text-2xl pl-8">
        SIANG FORUM
      </p>     
      <div className="flex items-center text-accent text-extrabold pr-8">
        <label className="swap swap-rotate">
          <input
            type="checkbox"
            onChange={handleToggle}
            checked={theme === "garden" ? false : true}
          />
          {theme === "garden" ? (
            <BsMoonStarsFill size={28} />
          ) : (
            <BsSunFill size={28} />
          )}
        </label>
      </div>
    </div>
  );
}
