import { Link, useNavigate } from "react-router-dom";
import { useRef, useState } from "react";

import { AppContext } from "../../context/app.context";
import Title from "./Title";
import { Transition } from "@headlessui/react";
import defaultAvatar from "/empty.jpg";
import { editPostTitle } from "../../services/post.services";
import { logoutUser } from "../../services/auth.service";
import { useContext } from "react";

export default function Header() {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();
  const ref = useRef();
  const { addToast, setAppState, userData, ...appState } =
    useContext(AppContext);

  const logout = async () => {
    await logoutUser();
    setAppState({
      ...appState,
      user: null,
      userData: null,
    });
    navigate("/");
  };

  return (
    <div className="flex w-full flex-col">
     <Title />
      <div className="bg-primary border-b-4 border-black border-double w-full lg:h-20 flex flex-col lg:flex-row justify-center items-center lg:justify-around">
        <div className=" flex flex-row gap-2 items-center">
          <div className="btn bg-inherit border-none rounded text-info">
            <Link to={"/"}>Home</Link>
          </div>
          <div className="btn bg-inherit border-none rounded text-info">
            {userData?.username !== null && <Link to={"members"}>Members</Link>}
          </div>
          <div className="btn bg-inherit border-none rounded text-info">
            {userData?.username !== null && (
              <Link to={"horoscope"}>Horoskope</Link>
            )}
          </div>
          {/* <div className="btn bg-inherit border-none rounded text-info">
            {userData?.username !== null && <Link to={"tarot"}>Tarot</Link>}
          </div> */}
        </div>

        <div className="flex flex-row lg:gap-6 items-center">
          <div>
            {appState?.user === null && (
              <ul className="menu menu-horizontal">
                <span className="btn bg-inherit border-none rounded text-info">
                  <Link to={"/login"}>Login</Link>
                </span>
                <span className="btn bg-inherit border-none rounded text-info">
                  <Link to={"/register"}>Register</Link>
                </span>
              </ul>
            )}
            {appState.user !== null && (
              <div>
                <div className="dropdown dropdown-end text-md text-info">
                  <span className="flex items-center text-info">
                    {`${userData?.username} ${" "}`}
                    <label
                      tabIndex={0}
                      className="btn btn-ghost btn-circle avatar ml-3"
                    >
                      <div className="w-10 rounded-full">
                        <img
                          src={
                            userData?.avatarUrl
                              ? userData?.avatarUrl
                              : defaultAvatar
                          }
                        />
                      </div>
                    </label>
                  </span>
                  <ul
                    tabIndex={0}
                    className="menu menu-compact dropdown-content mt-3 p-2 shadow text-info bg-gray-100 rounded-box w-52"
                  >
                    <li>
                      <Link
                        to={`/members/profile/${userData?.username}`}
                        className="justify-between"
                      >
                        Profile
                      </Link>
                    </li>
                    <li>
                      <span onClick={logout}>Logout</span>
                    </li>
                  </ul>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
