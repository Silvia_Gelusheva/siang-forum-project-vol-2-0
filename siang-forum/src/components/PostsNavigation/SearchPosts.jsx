export default function SearchPosts({ query, setQuery }) {
  return (
    <div className=" flex items-center pr-8">
      <input
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        type="text"
        placeholder="Search"
        className="input input-bordered w-full max-w-xs"
      />
    </div>
  );
}
