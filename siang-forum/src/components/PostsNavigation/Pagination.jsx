export default function Pagination({
  totalPosts,
  postsPerPage,
  setCurrentPage,
  currentPage,
}) {
  let pages = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pages.push(i);
  }

  return (
    <div>
      {pages.map((page, index) => {
        return (
          <button
            key={index}
            onClick={() => setCurrentPage(page)}
            className={
              page == currentPage
                ? "btn btn-sm bg-primary border-none text-secondary shadow-md hover:bg-primary active"
                : "btn btn-sm bg-gray-400 border-none text-secondary shadow-md hover:bg-primary"
            }
          >
            {page}
          </button>
        );
      })}
    </div>
  );
}
