export default function SortPosts({ mostRecent, mostLiked, mostCommented }) {
  return (
    <div className="dropdown dropdown-bottom dropdown-end ">
      <label
        tabIndex={0}
        className="btn bg-primary border-none shadow-sm mr-2 text-secondary"
      >
        Sort by
      </label>
      <ul
        tabIndex={0}
        className="dropdown-content menu p-2 shadow bg-primary text-secondary rounded-box w-52"
      >
        <li onClick={mostRecent}>
          <a>Most recent</a>
        </li>
        <li onClick={mostLiked}>
          <a>Most liked</a>
        </li>
        <li onClick={mostCommented}>
          <a>Most commented</a>
        </li>
      </ul>
    </div>
  );
}
