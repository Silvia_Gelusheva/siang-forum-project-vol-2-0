export default function AddPost({ toggle, setToggle }) {
  return (
    <button
      className="btn bg-neutral border-none shadow-sm mr-2 text-secondary"
      onClick={() => setToggle(!toggle)}
    >
      {toggle ? "Cancel" : "Add post"}
    </button>
  );
}
