import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useEffect, useState } from "react";

import About from "./views/About/About";
import { AppContext } from "./context/app.context";
import Blocked from "./views/Blocked/Blocked";
import CreatePost from "./components/CreatePost/CreatePost";
import Header from "./components/Header/Header";
import Horoscope from "./views/Horoscope/Horoscope";
import LandingPage from "./views/LandingPage/LandingPage";
import Login from "./views/Login/Login";
import { MembersTable } from "./views/Members/MembersTable";
import NotFound from "./views/NotFound/NotFound";
import Profile from "./views/Profile/Profile";
import Register from "./views/Register/Register";
import SinglePost from "./views/SinglePost/SinglePost";
// import Tarot from "./components/Tarot/Tarot";
import { auth } from "./firebase/config";
import { getUserById } from "./services/users.services";
import { useAuthState } from "react-firebase-hooks/auth";

function App() {
  const [user] = useAuthState(auth);
  const [appState, setAppState] = useState({
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null,
  });
  const [toast, setToasts] = useState([]);

  useEffect(() => {
    setAppState({
      ...appState,
      user: user ? { email: user.email, uid: user.uid } : null,
    });
  }, [user]);

  useEffect(() => {
    if (appState.user !== null) {
      getUserById(appState.user.uid)
        .then((userData) =>
          setAppState((appState) => ({ ...appState, userData }))
        )
        .catch((error) => addToast("error", error.message));
    }
  }, [appState.user]);

  if (appState.user !== null && appState.userData === null) {
    getUserById(appState.user.uid)
      .then((userData) =>
        setAppState((appState) => ({ ...appState, userData }))
      )
      .catch((error) => addToast("error", error.message));
  }

  const addToast = (type, message) => {
    const toast = {
      class: type === "error" ? "alert-error" : "alert-success",
      message,
    };
    setToasts((toasts) => [...toasts, toast]);
    setTimeout(() => {
      setToasts((toasts) => toasts.filter((t) => t !== toast));
    }, 6000);
  };

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setAppState, addToast }}>
        <div className="App">
          <Header />
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/createpost" element={<CreatePost />} />
            <Route path="/posts/:postId" element={<SinglePost />} />
            <Route path="/members" element={<MembersTable />} />
            <Route path="/members/profile/:username" element={<Profile />} />
            <Route path="/horoscope" element={<Horoscope />} />
            <Route path="/blocked" element={<Blocked />} />
            <Route path="/about" element={<About />} />
            <Route path="/*" element={<NotFound />} />
            {/* <Route path="/tarot" element={<Tarot />} /> */}
          </Routes>
          <div className="toasts">
            {toast.map((t, i) => (
              <div key={i} className={`alert ${t.class}`}>
                <div>
                  <span>{t.message}</span>
                </div>
              </div>
            ))}
          </div>
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
