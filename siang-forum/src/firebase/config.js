import { getAuth } from 'firebase/auth'
import { getDatabase } from 'firebase/database'
import { getStorage } from 'firebase/storage'
import { initializeApp } from 'firebase/app'

// Your web app's Firebase configuration

// const firebaseConfig = {

// 	apiKey: "AIzaSyCydjycgDFbnDWjuGPeqBqEOXCyZSUZWyo",
//   	authDomain: "my-app-df8d4.firebaseapp.com",
//   	databaseURL: "https://my-app-df8d4-default-rtdb.europe-west1.firebasedatabase.app",
//   	projectId: "my-app-df8d4",
//   	storageBucket: "my-app-df8d4.appspot.com",
//   	messagingSenderId: "981636724689",
//   	appId: "1:981636724689:web:bd0094421a4b167e9512f0"

//   };

const firebaseConfig = {

	apiKey: "AIzaSyCMtwfAN6s_wW2zpeITz96HAZ-r8KAuRw4",
	authDomain: "forum-app-f0094.firebaseapp.com",
	databaseURL: "https://forum-app-f0094-default-rtdb.europe-west1.firebasedatabase.app",
	projectId: "forum-app-f0094",
	storageBucket: "forum-app-f0094.appspot.com",
	messagingSenderId: "1004280120155",
	appId: "1:1004280120155:web:3715823ae23fb76c0b907e"
};



// Initialize Firebase
export const app = initializeApp(firebaseConfig)
export const auth = getAuth(app)
export const db = getDatabase(app)
export const storage = getStorage(app)

