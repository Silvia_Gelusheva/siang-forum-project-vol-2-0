/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
	// daisyui: {
	// 	themes: [
	// 		{
	// 			mytheme: {
	// 				primary: '#F3F4F6',
	// 				secondary: '#374151',
	// 				accent: '#aaf0d2',
	// 				neutral: "#E5E7EB",
	// 				'base-100': '#FFF',
	// 				info: '#999999',
	// 				success: '#22d3ee',
	// 				warning: '#fbbd23',
	// 				error: '#FFAA33',
	// 			},
	// 		},
	// 	],
	// },
	theme: {
		extend: {},
	},
	plugins: [require('daisyui')],
	daisyui: {
		themes: [ "garden", "coffee"],
	  },
}
