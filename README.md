### SiAng Forum Vol.2.0


### Description & Functionalities

SiAng Forum Project Vol. 2 is a FrontEnd Project - the upgraded version of an assignment for my Alpha JS Course at Telerik Academy. 

It is Single Page Application Forum System, where the users can:
- When not logged in - read only posts of others, logged in && read, comment, like posts;
- Sort and filter posts in the All Posts section;
- Create posts;
- Upvote/downvote the things that they like or dislike the most;
- Manage their Profile pages.
  Every user can delete their own posts and comments, can change and add personal information and upload avatar photo;
- Go to members profiles and get familiar with an additional information about them;
- Admins can block users, and unblock them after, delete all posts and comments;
- Check out their /yesterday/today/tomorrow astrological reading; 
- Get an Celtic Cross Tarot reading - *Tarot readings are still "under construction". 3 Card Reading, Fully explained Celtic Cross and a Daily Tarot Card are coming soon;*
- Fully responsive - in progress. 
<br>

### Project information

- JavaScript
- React.js
- CSS
- Daisy UI
- Tailwind CSS
- Firebase
- ESLint
<br>

### Setup

Open the root directory siang-forum folder. Install all packages by running:
`npm install`

Then run:
`npm run dev`
<br>

You can test the application and see for yourself what every user can do by login to the application as a regular user or Admin, using the following credentials:

*email:* user@test.com
*password:* 123456

*email:* admin@test.com
*password:* 123456

You can register a new user if you prefer.
<br>

#### **Landing page**

<img src='assets/ReadMe/Homepage.png' width='600px'>

<br/>

#### **Landing page /dark mode/**

<img src='assets/ReadMe/Homepage-dark_theme.png' width='600px'>

<br/>

#### **Register**

<img src='assets/ReadMe/Register.png' width='600px'> 

<br/>

#### **Login**

<img src='assets/ReadMe/Login.png' width='600px'> 

<br/>

#### **Single Post**

<img src='assets/ReadMe/SinglePost.png' width='600px'> 

<br/>

#### **Create post**

<img src='assets/ReadMe/CreatePost.png' width='600px'> 

<br/>

#### **Members Table**

<img src='assets/ReadMe/Members.png' width='600px'> 

<br/>

#### **Profile**

<img src='assets/ReadMe/Profilepage.png' width='600px'> 

<br/>

<img src='assets/ReadMe/Profilepage2.png' width='600px'> 

<br/>

#### **Horoscope**

<img src='assets/ReadMe/Horoscope.png' width='600px'> 

<br/>



